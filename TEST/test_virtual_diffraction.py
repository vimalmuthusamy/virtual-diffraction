# coding: utf-8
# Testing program for Virtual diffraction routines

import pytest
import numpy as np
import sys, os
cwd = os.getcwd() # Current working directory path
os.chdir("../") # Changing to the previous directory
sys.path.append(os.getcwd()) # Changing the path in the sys. Using only chdir does not allow for file imports
from Virtual_diffraction import get_rotated_coordinates
from Virtual_diffraction import get_real_atom_coordinates_and_basis_vectors
from Virtual_diffraction import get_reciprocal_basis_vectors
from Virtual_diffraction import get_atom_coordinates
from Virtual_diffraction import get_reciprocal_coordinates_intersecting_ewald_sphere
from Virtual_diffraction import get_diffraction_angle
from Virtual_diffraction import get_atomic_scattering_factor
os.chdir(cwd) # Changing back to the current directory
from itertools import product
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# @pytest.mark.skip("Skipping Test case")
def test_get_rotated_coordinates_no_rotation():
    """
        Description:
        ===========
        Tests whether the same coordinates results after applying rotation matrices 
        for "0" angle rotation
    """
    print(test_get_rotated_coordinates_no_rotation.__doc__)
    
    x = np.arange(3)
    Atomic_coordinates = np.array(list(product(x, repeat = 3))) # Combinations for x, y & z axis
    Euler_angles = np.array([0,0,0])
    Rotated_coordinates = get_rotated_coordinates(Euler_angles,Atomic_coordinates)
    
    assert np.allclose(Atomic_coordinates,Rotated_coordinates)
    
# @pytest.mark.skip("Skipping Test case")
def test_get_rotated_coordinates_180_rotation(get_atomic_coordinates):
    """
        Description:
        ===========
        Tests whether the 180 degree flipping simple cubic structure about y axis 
        results in reversed x coordinates and z coordinates
    """
    print(test_get_rotated_coordinates_180_rotation.__doc__)
    
    Atomic_coordinates = get_atomic_coordinates
    Euler_angles = np.radians(np.array([0,180,0])) # 180 degree rotation about y
    Rotated_coordinates = get_rotated_coordinates(Euler_angles,Atomic_coordinates)
    
    Reference_flipped_x_z_coordinates = np.copy(Atomic_coordinates)
    Reference_flipped_x_z_coordinates[:,0] *= -1
    Reference_flipped_x_z_coordinates[:,2] *= -1

    assert np.allclose(Reference_flipped_x_z_coordinates,Rotated_coordinates)

# @pytest.mark.skip("Skipping Test case")
def test_get_direct_space_coordinates_basis():
    """
        Description:
        ===========
        Tests the get_real_atom_coordinates_and_basis_vectors function with the 
        reference values
    """
    print(test_get_direct_space_coordinates_basis.__doc__)
    
    Crystal_structure = ["sc","fcc","bcc"]
    Lattice_constant = 1
    Reference_coordinates = [np.array([0,0,0]),
                            Lattice_constant * np.array([[0,0,0],[1/2,1/2,0],[0,1/2,1/2],[1/2,0,1/2]]),
                            Lattice_constant * np.array([[0,0,0],[1/2,1/2,1/2]])]
    Reference_basis_vectors = [np.array([[1,0,0],[0,1,0],[0,0,1]]),
                               Lattice_constant * np.array([[0,1/2,1/2],[1/2,0,1/2],[1/2,1/2,0]]),
                               Lattice_constant * np.array([[-1/2,1/2,1/2],[1/2,-1/2,1/2],[1/2,1/2,-1/2]])]
    
    for i,structure in enumerate(Crystal_structure):
        Real_atom_coordinates, Real_basis_vectors = get_real_atom_coordinates_and_basis_vectors(structure,Lattice_constant)

        assert np.allclose(Real_atom_coordinates,Reference_coordinates[i])
        assert np.allclose(Real_basis_vectors,Reference_basis_vectors[i])

# @pytest.mark.skip("Skipping Test case")
def test_get_reciprocal_basis_vectors_simple_cubic():
    """
        Description:
        ===========
        Tests whether simple cubic structure in real space results in simple cubic 
        structure in reciprocal space
    """
    print(test_get_reciprocal_basis_vectors_simple_cubic.__doc__)

    Real_simple_cubic_basis_vectors = np.array([[1,0,0],[0,1,0],[0,0,1]])
    Reciprocal_simple_cubic_basis_vectors = get_reciprocal_basis_vectors(Real_simple_cubic_basis_vectors)
    
    assert np.allclose(Real_simple_cubic_basis_vectors, Reciprocal_simple_cubic_basis_vectors)

# @pytest.mark.skip("Skipping Test case")
def test_get_reciprocal_basis_vectors_fcc():
    """
        Description:
        ===========
        Tests whether fcc structure in real space results in bcc
        structure in reciprocal space
    """
    print(test_get_reciprocal_basis_vectors_fcc.__doc__)
    
    Real_fcc_basis_vectors = np.array([[0,1/2,1/2],[1/2,0,1/2],[1/2,1/2,0]])
    Reference_bcc_basis_vectors = np.array([[-1/2,1/2,1/2],[1/2,-1/2,1/2],[1/2,1/2,-1/2]])
    Reference_bcc_basis_vectors = Reference_bcc_basis_vectors / abs(np.max(Reference_bcc_basis_vectors)) # Since only the structure is verified
    Reciprocal_fcc_basis_vectors = get_reciprocal_basis_vectors(Real_fcc_basis_vectors)
    
    assert np.allclose(Reference_bcc_basis_vectors, Reciprocal_fcc_basis_vectors)

# @pytest.mark.skip("Skipping Test case")
def test_get_reciprocal_basis_vectors_bcc():
    """
        Description:
        ===========
        Tests whether bcc structure in real space results in fcc
        structure in reciprocal space
    """
    print(test_get_reciprocal_basis_vectors_fcc.__doc__)
    
    Real_bcc_basis_vectors = np.array([[-1/2,1/2,1/2],[1/2,-1/2,1/2],[1/2,1/2,-1/2]])
    Reference_fcc_basis_vectors = np.array([[0,1/2,1/2],[1/2,0,1/2],[1/2,1/2,0]]) 
    Reference_fcc_basis_vectors = Reference_fcc_basis_vectors / abs(np.max(Reference_fcc_basis_vectors)) # Since only the structure is verified; np.max -> To avoid divide by 0
    Reciprocal_bcc_basis_vectors = get_reciprocal_basis_vectors(Real_bcc_basis_vectors)
    
    assert np.allclose(Reference_fcc_basis_vectors, Reciprocal_bcc_basis_vectors)

# @pytest.mark.skip("Skipping Test case")
def test_get_atom_coordinates_within_sphere():
    """
        Description:
        ===========
        Tests the atomic coordinates generation function with basis vectors and comparing
        it with the reference coordinates within the sphere for the simple cubic case
    """
    print(test_get_atom_coordinates_within_sphere.__doc__)
    
    Real_simple_cubic_basis_vectors = np.array([[1,0,0],[0,1,0],[0,0,1]])
    x_points, y_points, z_points = np.arange(-10,10), np.arange(-10,10), np.arange(-10,10)
    Ewald_sphere_radius = 2 # Arbitrary
    Scaling_factor = 1
    Box_dimensions = [[-10,-10,-10],[10,10,10]]
    Reference_coordinates = np.array(list(product(np.arange(-2*Ewald_sphere_radius,(2*Ewald_sphere_radius+1)), repeat = 3)))
    
    Coordinates_within_ewald_sphere_simple_cubic = get_atom_coordinates(x_points,y_points,z_points,Real_simple_cubic_basis_vectors,Box_dimensions,Scaling_factor,Ewald_sphere_radius)
    
    assert np.allclose(Coordinates_within_ewald_sphere_simple_cubic,Reference_coordinates)
    
    
# @pytest.mark.skip("Skipping Test case")
def test_get_reciprocal_coordinates_intersecting_ewald_sphere_source_beam():
    """
        Description:
        ===========
        Tests whether reciprocal lattice points lies on the Ewald sphere -> Only the source beam should result
    """
    print(test_get_reciprocal_coordinates_intersecting_ewald_sphere_source_beam.__doc__)
    
    Ewald_sphere_radius = 5
    x = np.arange(3)
    Coordinates = np.array(list(product(x, repeat = 3))) # Combinations for x, y & z axis
    Incident_wave_vector = np.array([Ewald_sphere_radius,0,0])
    Bandwidth_ewald_sphere_surface = 0 # For source beam
   
    Intersecting_coordinates, Intersecting_diffracted_wave_vector, Intersecting_indices = get_reciprocal_coordinates_intersecting_ewald_sphere(Coordinates,Incident_wave_vector,Ewald_sphere_radius,Bandwidth_ewald_sphere_surface)
    
    assert np.allclose(Intersecting_coordinates,Coordinates[0]) # The first coordinate is the source beam
    assert Intersecting_indices == 0
    
# @pytest.mark.skip("Skipping Test case")
def test_get_reciprocal_coordinates_intersecting_ewald_sphere_all_points():
    """
        Description:
        ===========
        Tests whether reciprocal lattice points lies on the Ewald sphere -> Only the source beam should result
    """
    print(test_get_reciprocal_coordinates_intersecting_ewald_sphere_all_points.__doc__)
    
    Ewald_sphere_radius = 5
    x = np.arange(3)
    Coordinates = np.array(list(product(x, repeat = 3))) # Combinations for x, y & z axis
    Incident_wave_vector = np.array([Ewald_sphere_radius,0,0])
    Bandwidth_ewald_sphere_surface = 5 
   
    Intersecting_coordinates, Intersecting_diffracted_wave_vector, Intersecting_indices = get_reciprocal_coordinates_intersecting_ewald_sphere(Coordinates,Incident_wave_vector,Ewald_sphere_radius,Bandwidth_ewald_sphere_surface)
    
    assert np.allclose(Intersecting_coordinates,Coordinates) # The first coordinate is the source beam
    assert len(Intersecting_indices) == len(Coordinates)

# @pytest.mark.skip("Skipping Test case")
def test_get_diffraction_angle():
    """
        Description:
        ===========
        Tests computed diffraction angle with the known value
    """
    print(test_get_diffraction_angle.__doc__)
    
    Lambda = 2
    Coordinates = np.array([[0,0,0],[0,0,1]])
    Reference_angles = np.radians(np.array([0,90]))
    
    Diffraction_angles = get_diffraction_angle(Coordinates,Lambda)
    
    assert np.allclose(Diffraction_angles,Reference_angles)

# @pytest.mark.skip("Skipping Test case")
def test_get_atomic_scattering_factor_electron():
    """
        Description:
        ===========
        Tests computed atomic scattering factors with the known value; source beam - Electron
    """
    print(test_get_atomic_scattering_factor_electron.__doc__)
    
    Parameter_a = np.ones(5)
    Parameter_b = np.ones(5)
    Parameter_c = 10
    Diffraction_angles = np.array([0])
    Source_beam = "electron"
    Lambda = 1
    
    Atomic_scattering_factor = get_atomic_scattering_factor(Diffraction_angles,Lambda,Parameter_a,Parameter_b,Parameter_c,Source_beam)
    
    assert Atomic_scattering_factor == 5 # "5" -> Sum of parameter "a"

# @pytest.mark.skip("Skipping Test case")
def test_get_atomic_scattering_factor_x_ray():
    """
        Description:
        ===========
        Tests computed atomic scattering factors with the known value; source beam - X-ray
    """
    print(test_get_atomic_scattering_factor_x_ray.__doc__)
    
    Parameter_a = np.ones(4)
    Parameter_b = np.ones(4)
    Parameter_c = 10
    Diffraction_angles = np.array([0])
    Source_beam = "x-ray"
    Lambda = 1
    
    Atomic_scattering_factor = get_atomic_scattering_factor(Diffraction_angles,Lambda,Parameter_a,Parameter_b,Parameter_c,Source_beam)
    
    assert Atomic_scattering_factor == 14 # "14" -> Sum of parameter "a" and parameter "c"


# @pytest.mark.skip("Skipping Test case")
def test_get_atom_coordinates_within_sphere_fcc():
    """
        Description:
        ===========
        Tests the atomic coordinates generation function with basis vectors for the FCC case
    """
    print(test_get_atom_coordinates_within_sphere_fcc.__doc__)
    
    Real_fcc_basis_vectors = np.array([[0,1/2,1/2],[1/2,0,1/2],[1/2,1/2,0]]) 
    x_points, y_points, z_points = np.arange(-10,10), np.arange(-10,10), np.arange(-10,10)
    Ewald_sphere_radius = 0.5 # Arbitrary
    Scaling_factor = 1
    Box_dimensions = [[-10,-10,-10],[10,10,10]]
    
    Coordinates_within_ewald_sphere_fcc = get_atom_coordinates(x_points,y_points,z_points,Real_fcc_basis_vectors,Box_dimensions,Scaling_factor,Ewald_sphere_radius)
    
    fig = plt.figure()
    ax = fig.add_subplot(111,projection = "3d")
    ax.scatter(Coordinates_within_ewald_sphere_fcc[:,0],Coordinates_within_ewald_sphere_fcc[:,1],Coordinates_within_ewald_sphere_fcc[:,2], s = 100)
    ax.set(xlabel = "x coordinate " + r"($\AA$)",ylabel = "y coordinate " + r"($\AA$)",zlabel = "z coordinate " + r"($\AA$)")
    ax.set_title("Atom coordinates for FCC using basis vectors", weight = "bold")
    ax.grid()
    plt.savefig("test_get_atom_coordinates_within_sphere_fcc.png")
    plt.close()
    

# @pytest.mark.skip("Skipping Test case")
def test_get_atom_coordinates_within_sphere_bcc():
    """
        Description:
        ===========
        Tests the atomic coordinates generation function with basis vectors for the BCC case
    """
    print(test_get_atom_coordinates_within_sphere_bcc.__doc__)
    
    Real_bcc_basis_vectors = np.array([[-1/2,1/2,1/2],[1/2,-1/2,1/2],[1/2,1/2,-1/2]])
    x_points, y_points, z_points = np.arange(-10,10), np.arange(-10,10), np.arange(-10,10)
    Ewald_sphere_radius = 0.5 # Arbitrary
    Scaling_factor = 1
    Box_dimensions = [[-10,-10,-10],[10,10,10]]
    
    Coordinates_within_ewald_sphere_fcc = get_atom_coordinates(x_points,y_points,z_points,Real_bcc_basis_vectors,Box_dimensions,Scaling_factor,Ewald_sphere_radius)
    
    fig = plt.figure()
    ax = fig.add_subplot(111,projection = "3d")
    ax.scatter(Coordinates_within_ewald_sphere_fcc[:,0],Coordinates_within_ewald_sphere_fcc[:,1],Coordinates_within_ewald_sphere_fcc[:,2], s = 100)
    ax.grid()
    ax.set(xlabel = "x coordinate " + r"($\AA$)",ylabel = "y coordinate " + r"($\AA$)",zlabel = "z coordinate " + r"($\AA$)")
    ax.set_title("Atom coordinates for BCC using basis vectors", weight = "bold")
    plt.savefig("test_get_atom_coordinates_within_sphere_bcc.png")
    plt.close()
    