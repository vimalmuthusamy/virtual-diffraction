# Program to calculate the intensities of diffraction for each reciprocal lattice point

"""
+++++++ Units ++++++++
distance = Angstroms
angle = radians
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from tqdm import tqdm

####################################################################################################################################################################
#                                                                   REFERENCES
####################################################################################################################################################################
# 1) Coleman, Shawn & Mirzaei Sichani, Mehrdad & Spearot, Douglas. (2014). A Computational Algorithm to Produce Virtual X-ray and Electron Diffraction Patterns 
#    from Atomistic Simulations. JOM: the journal of the Minerals, Metals & Materials Society. 66. 10.1007/s11837-013-0829-3.
# 2)  L-M . Peng, G.Ren,S.L.Dudarev and M . J. Whelan, Acta Cryst. (1996), Robust Parameterization of Elastic and Absorptive Electron Atomic Scattering Factors,
#    A52, 257-276
# 3)  A.G.Fox, M.A.O’Keefe and, M.A.Tabbernor, Relativistic Hartree–Fock X-ray and Electron Atomic Scattering Factors at High Angles, Acta Cryst. (1989). A45, 
#    786-793
# 4) P. A. Doyle and P. S. Turner, Relativistic Hartree-Fock X-ray and electron scattering factors, Acta Cryst. (1989). A45, 786-793
####################################################################################################################################################################

def get_rotated_coordinates(Euler_angles, Coordinates):
    """
        Description:
        ===========
        Calculates the rotation matrix for given rotation angles (Euler angles) and calculates the 
        rotated atomic coordinates
        
        Input:
        =====
        1) Euler_angles = Orientation in terms of Euler angles in radians; Array size -> (1,3)
        2) Coordinates = Atomic coordinates; Array size -> (No of atoms, 3)
        
        Output:
        ======
        1) Rotated_coordinates = Atomic coordinates applied with rotation matrices; Array size -> (No of atoms, 3)
    """
    Rotation_matrix = (np.array([[np.cos(Euler_angles[2]),-np.sin(Euler_angles[2]),0],[np.sin(Euler_angles[2]),np.cos(Euler_angles[2]),0],[0,0,1]]) @ 
                    np.array([[np.cos(Euler_angles[1]),0,-np.sin(Euler_angles[1])],[0,1,0],[np.sin(Euler_angles[1]),0,np.cos(Euler_angles[1])]]) @ 
                    np.array([[1,0,0],[0,np.cos(Euler_angles[0]),-np.sin(Euler_angles[0])],[0,np.sin(Euler_angles[0]),np.cos(Euler_angles[0])]]))
    
    Rotated_coordinates = np.array([Rotation_matrix @ coordinate for coordinate in Coordinates])

    return Rotated_coordinates

def get_real_atom_coordinates_and_basis_vectors(Crystal_structure, Lattice_constant):
    """
        Description:
        ===========
        Returns the atomic coordinates & the basis vectors of the real lattice 
        based on the Crystal structure
        
        Input:
        =====
        1) Crystal_structure = Abbreviation of cubic crystal structures; [sc,bcc,fcc]
        2) Lattice_constant = Lattice constant of the crystal structure; Array size -> Scalar
        
        Output:
        ======
        1) Real_atom_coordinates = Atomic coordinates in direct space for the given crystal structure; 
                                   Array size -> (No of atoms in the unit cell, 3)
        2) Real_basis_vectors = Basis vectors of the direct space unit cell; Array size -> (3,3)
    """
    Real_atom_coordinates = {"sc": np.array([0,0,0]),
                             "fcc": Lattice_constant * np.array([[0,0,0],[1/2,1/2,0],[0,1/2,1/2],[1/2,0,1/2]]),
                             "bcc": Lattice_constant * np.array([[0,0,0],[1/2,1/2,1/2]])} # Lattice points in a cubic unit cell
    Real_basis_vectors = {"sc": np.array([[1,0,0],[0,1,0],[0,0,1]]),
                          "fcc": Lattice_constant * np.array([[0,1/2,1/2],[1/2,0,1/2],[1/2,1/2,0]]),
                          "bcc": Lattice_constant * np.array([[-1/2,1/2,1/2],[1/2,-1/2,1/2],[1/2,1/2,-1/2]])}

    return Real_atom_coordinates[Crystal_structure], Real_basis_vectors[Crystal_structure]

def get_reciprocal_basis_vectors(Real_basis_vectors):
    """
        Description:
        ===========
        Calculates the reciprocal basis vectors for the given real basis vectors
        
        Input:
        =====
        1) Real_basis_vectors = Basis vectors of the direct space unit cell; Array size -> (3,3)
        
        Output:
        ======
        1) Reciprocal_basis_vectors = Basis vectors of the reciprocal space unit cell; Array size -> (3,3)
    """
    Volume_real = np.dot(Real_basis_vectors[0],np.cross(Real_basis_vectors[1], Real_basis_vectors[2]))
    Reciprocal_basis_vectors = np.zeros_like(Real_basis_vectors)
    Reciprocal_basis_vectors[0] = np.cross(Real_basis_vectors[1],Real_basis_vectors[2]) / Volume_real
    Reciprocal_basis_vectors[1] = np.cross(Real_basis_vectors[2],Real_basis_vectors[0]) / Volume_real
    Reciprocal_basis_vectors[2] = np.cross(Real_basis_vectors[0],Real_basis_vectors[1]) / Volume_real
    
    return Reciprocal_basis_vectors

def get_atom_coordinates(x_points, y_points, z_points, Basis_vector, Box_dimensions, Scaling_factor, Ewald_sphere_radius):
    """
        Description:
        ===========
        Calculates the atom coordinates based on the basis vectors for the interval of points 
        in x_points, y_points & z_points within +/- twice the Ewald sphere radius
        
        Input:
        =====
        1) x_points = Array of points for generating the lattice points in x direction; Array size -> (1,No of points)
        2) y_points = Array of points for generating the lattice points in y direction; Array size -> (1,No of points)
        3) z_points = Array of points for generating the lattice points in z direction; Array size -> (1,No of points)
        4) Basis_vector = Basis vectors of an unit cell; Array size -> (3,3)
        5) Box_dimensions = Simulation box dimensions; Array size -> (2,3)
        6) Scaling_factor = Factor to generate high density points within the reciprocal volume; Array size -> Scalar
        7) Ewald_sphere_radius = Radius of the Ewald sphere constructed; Array size -> Scalar
                            
        Output:
        ======
        1) Coordinates  = Atomic coordinates; Array size -> (No of atoms, 3)
    """
    
    Coordinates = np.array([])
    for i in tqdm(x_points):
        for j in y_points:
            for k in z_points:
                coordinate = (i*Basis_vector[0] + j*Basis_vector[1] + k*Basis_vector[2]) * Scaling_factor
                if ((-Ewald_sphere_radius*4) <= coordinate[0] <= Ewald_sphere_radius*4) and ((-Ewald_sphere_radius*4) <= coordinate[1] <= (Ewald_sphere_radius*4)) and ((-Ewald_sphere_radius*4) <= coordinate[2] <= (Ewald_sphere_radius*4)): # Restricting the lattice points generation within +/- 2*Ewald sphere radius
                    Coordinates = np.append(Coordinates,coordinate) 
                
    return Coordinates.reshape(-1,3) # Reshaping array to be a per atom vector

def get_reciprocal_coordinates_intersecting_ewald_sphere(Reciprocal_atom_coordinates_rotated, Incident_wave_vector, Ewald_sphere_radius, Delta_ewald_sphere):
    """
        Description:
        ===========
        Checks whether the reciprocal lattice vectors intersects the Ewald sphere surface and 
        returns the reciprocal coordinates that lies on the surface
        
        Input:
        =====
        1) Reciprocal_atom_coordinates_rotated = Reciprocal atomic coordinates applied with rotation matrices; Array size -> (No of atoms, 3)
        2) Incident_wave_vector = Incident wave vector of the source beam; Array size -> (1,3)
        3) Ewald_sphere_radius = Inverse of the source beam wavelength; Array size -> Scalar
        4) Delta_ewald_sphere = Bandwidth across the Ewald sphere surface; Array size -> Scalar
        
        Output:
        ======
        1) Intersecting_reciprocal_coordinates = Reciprocal atomic coordinates that within the bandwidth of the Ewald sphere surface;
                                                 Array size -> (No of lattice points lying on Ewald sphere surface, 3)
        2) Intersecting_diffracted_wave_vector = Diffracted wave vector of the lattice points lying on the Ewald sphere surface;
                                                 Array size -> (No of lattice points lying on Ewald sphere surface, 3)
        3) Intersecting_indices = Indices of the lattice points lying on the Ewald sphere surface; 
                                  Array size -> (1,No of lattice points lying on Ewald sphere surface)
    """
    Diffracted_wave_vector = Reciprocal_atom_coordinates_rotated - Incident_wave_vector # K = Kd - Ki
    Diffracted_wave_vector_norm = np.linalg.norm(Diffracted_wave_vector, axis = 1)
    Intersecting_indices = np.argwhere(((Diffracted_wave_vector_norm <= (Ewald_sphere_radius + Delta_ewald_sphere)) & (Diffracted_wave_vector_norm >= (Ewald_sphere_radius - Delta_ewald_sphere)))).flatten()
    Intersecting_reciprocal_coordinates = Reciprocal_atom_coordinates_rotated[Intersecting_indices]
    Intersecting_diffracted_wave_vector = Diffracted_wave_vector[Intersecting_indices]
    
    return Intersecting_reciprocal_coordinates, Intersecting_diffracted_wave_vector, Intersecting_indices

def get_diffraction_angle(Reciprocal_atom_coordinates, Lambda):
    """
        Description:
        ===========
        Calculates the diffraction angle for each reciprocal lattice point using the relationship 
        between wavelength and distance between planes (Bragg's relation)
        
        Input:
        =====
        1) Reciprocal_atom_coordinates = Coordinates of the reciprocal lattice points; Array size -> (No of reciprocal lattice points, 3)
        2) Lambda = Wavelength of the source beam; Array size -> Scalar
        
        Output:
        ======
        1) Diffraction_angle = Angle of diffraction for each reciprocal lattice point; Array size -> (No of reciprocal lattice points, 3)
    """
    Sin_theta = (Lambda/2) * np.linalg.norm(Reciprocal_atom_coordinates.reshape(-1,3), axis = 1)
    Sin_theta = np.select([Sin_theta <= 1, Sin_theta > 1], [Sin_theta, 0]) # To avoid invalid value warning in np.arcsin() if the value is greater than 1s
    Diffraction_angle = np.arcsin(Sin_theta) # One angle for each reciprocal lattice point; 2*sin(theta) / Lambda = |K|; reshape(-1,3) is done for arrays with only one coordinate

    return Diffraction_angle

def get_atomic_scattering_factor(Diffraction_angles, Lambda, Atomic_scattering_factor_parameter_a, Atomic_scattering_factor_parameter_b, Atomic_scattering_factor_parameter_c, Source_beam):
    """
        Description:
        ===========
        Calculates the atomic scattering factors based on the parameterized analytical expression
        for x-ray diffraction by Doyle & Turner or the summation of 5 Gaussian functions parameterized by Peng et al
        
        Input:
        =====
        1) Diffraction_angles = Angle of diffraction for each reciprocal lattice point; Array size -> (No of reciprocal lattice points, 3)
        2) Lambda = Wavelength of the source beam; Array size -> Scalar
        3) Atomic_scattering_factor_parameter_a = Parameterized atomic scattering factor (a) from references; Array size -> (1,5) for electron / Array size -> (1,4) for X-ray
        4) Atomic_scattering_factor_parameter_b = Parameterized atomic scattering factor (b) from references; Array size -> (1,5) for electron / Array size -> (1,4) for X-ray
        5) Atomic_scattering_factor_parameter_c = Parameterized atomic scattering factor (c) from references; Array size -> (1,5) for electron / Array size -> (1,4) for X-ray
        6) Source_beam = Incident beam used; [x-ray, electron]
        
        Output:
        ======
        1) Atomic_scattering_factors = Computed atomic scattering factors from the parameterized analytical expressions for the corresponding source beam; 
                                       Array size -> (1, No of reciprocal lattice points)
    """
    if Source_beam == "x-ray":
        Exponent_part = np.exp( - Atomic_scattering_factor_parameter_b.reshape(4,1) * np.tile(((np.sin(Diffraction_angles) / Lambda)**2),(4,1))) # np.tile -> Repeats the 1D array "n" times to form a 2D array !!
        Atomic_scattering_factors = np.sum(Atomic_scattering_factor_parameter_a.reshape(4,1) * Exponent_part, axis = 0) + Atomic_scattering_factor_parameter_c
    
    else:
        Exponent_part = np.exp( - Atomic_scattering_factor_parameter_b.reshape(5,1) * np.tile(((np.sin(Diffraction_angles) / Lambda)**2),(5,1))) # np.tile -> Repeats the 1D array "n" times to form a 2D array !!
        Atomic_scattering_factors = np.sum(Atomic_scattering_factor_parameter_a.reshape(5,1) * Exponent_part, axis = 0)  # reshaping into 5 since analytical expressions for electron diffraction contains 5 parameters
    return Atomic_scattering_factors

def get_structure_factor_intensities(Reciprocal_atom_coordinates,Real_atom_coordinates,Atomic_scattering_factors):
    """
        Description:
        ===========
        Computes the structure factor and intensities by rotating the reciprocal lattice for the given angles.
        
        Input:
        =====
        1) Reciprocal_atom_coordinates = Coordinates of the reciprocal lattice points; Array size -> (No of reciprocal lattice points, 3)
        2) Real_atom_coordinates = Atomic coordinates in direct space for the given crystal structure; Array size -> (No of atoms in the unit cell, 3)
        3) Atomic_scattering_factors = Computed atomic scattering factors from the parameterized analytical expressions for the corresponding source beam; 
                                    Array size -> (1, No of reciprocal lattice points)
        
        Output:
        ======
        1) Structure_factor = Structure factor for the reciprocal lattice points; Array size -> (1, No of reciprocal lattice points)
        2) Intensities = Intensities for the reciprocal lattice points; Array size -> (1, No of reciprocal lattice points)
    """
    
    Structure_factor = np.zeros_like(Atomic_scattering_factors, dtype=complex)
    Intensities = np.zeros_like(Atomic_scattering_factors)
    
    for i,reciprocal_vector in tqdm(enumerate(Reciprocal_atom_coordinates)):
        Imaginary_part = 2*np.pi*np.dot(Real_atom_coordinates,reciprocal_vector) # Bigger array first in np.dot() sequence
        Exponent_term = np.zeros_like(Imaginary_part, dtype = complex) # Creating an empty complex number array
        Exponent_term.imag = Imaginary_part
        Structure_factor[i] = np.sum(Atomic_scattering_factors[i] * np.exp(Exponent_term)) # Structure factor (Resultant of all diffracted waves) for one reciprocal lattice point
        Intensities[i] = np.vdot(Structure_factor[i],Structure_factor[i]).real # Calculates the dot product of the complex number with its conjugate; np.vdot() takes care of complex conjugates; Only real values results -> Warning can be ignored since imaginary part is "0"
        
    return Structure_factor, Intensities
    
if __name__=="__main__":
        
    """
    =============================================================================================================================================================================================================
                                                                            Input Parameters
    =============================================================================================================================================================================================================
    """

    Lattice_constant = 1#4.05 # Aluminum lattice constant
    Crystal_structure = "bcc"#"fcc" # sc -> Simple cubic; fcc -> Face Centered Cubic; bcc -> Body Centered Cubic
    Box_dimensions = np.array([[-10,-10,-10],[10,10,10]]) # Row 1 -> Box start dimensions, Row 2 -> Box end dimensions
    Interval = 1
    x_points = np.arange((Box_dimensions[0,0] - Box_dimensions[1,0])*2,(Box_dimensions[1,0]*5),Interval)
    y_points = np.arange((Box_dimensions[0,1] - Box_dimensions[1,1])*2,(Box_dimensions[1,1]*5),Interval)
    z_points = np.arange((Box_dimensions[0,2] - Box_dimensions[1,2])*2,(Box_dimensions[1,2]*5),Interval)
    Rotation_angle_about_z = np.radians(np.arange(0,1,1)) # Rotating completely with an increment of 1 degree
    
    Scaling_factor = 1 # Reciprocal coordinate scaling factor to generate high density points within the reciprocal volume !
    ###### X-ray
    Source_beam = "x-ray"
    Lambda = 1.542 # Angstrom; Wavelength of the beam x-ray - 1.542 Angstrom; Electron - 15 pm
    
    ###### Electron
    # Source_beam = "electron"
    # Lambda = 0.15 # Angstrom; Wavelength of the beam x-ray - 1.542 Angstrom; Electron - 15 pm 
       
    Ewald_sphere_radius = 1/Lambda # Ewald sphere - geometrical interpretation of the reciprocal lattice points lying on the surface of the sphere satisfying diffraction conditions
    Delta_ewald_sphere = 1#1e-3 #0.005 * Ewald_sphere_radius # 5% of the Ewald_sphere_radius is taken as the range at the Ewald sphere surface to check intersection
    Zone_axis = np.array([1,0,0]) 
    Incident_wave_vector = Zone_axis * Ewald_sphere_radius # Direction of the incident beam 
    
    if Source_beam == "electron":
        ##########################################################################################################################################################################################
        #                                                                            Electron diffraction
        ##########################################################################################################################################################################################                                                                          
                                                                                    
        Atomic_scattering_factor_parameter_a = np.array([0.2390, 0.6573,1.2011, 2.5586, 1.2312]) # a1, a2, a3, a4, a5 for Al from Peng et al (1996)
        Atomic_scattering_factor_parameter_b = np.array([0.3138, 2.1063, 10.4163, 34.4552, 98.5344]) # b1, b2, b3, b4 & b5 for Al from Peng et al (1996)
        Atomic_scattering_factor_parameter_c = 0 # Defined just for using the same code
    else:
        ##########################################################################################################################################################################################
        #                                                                            X-ray diffraction
        ##########################################################################################################################################################################################                                                                          
        
        Atomic_scattering_factor_parameter_a = np.array([6.4202, 1.9002, 1.5936, 1.9646]) # a1, a2, a3, a4 for Al from Doyle & Turner
        Atomic_scattering_factor_parameter_b = np.array([3.0387, 0.7426, 31.5472, 85.0886]) # b1, b2, b3, b4 for Al from Doyle & Turner
        Atomic_scattering_factor_parameter_c = 1.1151

    Real_atom_coordinates, Real_basis_vectors = get_real_atom_coordinates_and_basis_vectors(Crystal_structure, Lattice_constant)
    No_of_atoms = len(Real_atom_coordinates)
    Reciprocal_basis_vectors = get_reciprocal_basis_vectors(Real_basis_vectors)
    Unit_value_reciprocal_space = np.amax(abs(Reciprocal_basis_vectors[0])) # This is equivalent to the 1 in Miller index

    Reciprocal_box_dimensions = np.divide(np.ones_like(Box_dimensions),Box_dimensions, out = np.zeros_like(Box_dimensions, dtype = float), where= Box_dimensions!=0 )
    Reciprocal_atom_coordinates_all = get_atom_coordinates(x_points,y_points,z_points,Reciprocal_basis_vectors, Reciprocal_box_dimensions, Scaling_factor, Ewald_sphere_radius)

    Reciprocal_atom_coordinates_actual, Diffracted_wave_vector_actual,Intersecting_indices = get_reciprocal_coordinates_intersecting_ewald_sphere(Reciprocal_atom_coordinates_all,Incident_wave_vector,Ewald_sphere_radius,Delta_ewald_sphere)

    Diff_pattern, ax = plt.subplots(figsize = (20,20)) # plot initialization
    Diffracting_coordinates_miller_indices = np.array([]) # Array initialization
    Diffraction_angles_of_diffracting_coordinates = np.array([]) # Array initialization
    Intensities_of_diffracting_coordinates = np.array([]) # Array initialization
    
    """
    =============================================================================================================================================================================================================
                                                                            Structure factors & Intensities
    =============================================================================================================================================================================================================
    """
    for Rotation_angle in tqdm(Rotation_angle_about_z):
        
        Reciprocal_atom_coordinates_rotated = get_rotated_coordinates(np.array([0,0,Rotation_angle]), Reciprocal_atom_coordinates_all)
        Reciprocal_atom_coordinates, Diffracted_wave_vector,Intersecting_indices = get_reciprocal_coordinates_intersecting_ewald_sphere(Reciprocal_atom_coordinates_rotated,Incident_wave_vector,Ewald_sphere_radius,Delta_ewald_sphere)
        Diffraction_angles = get_diffraction_angle(Reciprocal_atom_coordinates_all[Intersecting_indices], Lambda) # Since the diffraction angle should depend on only the (hkl) ie) reciprocal lattice point & not with the rotated reciprocal coordinate !!!
        
        Diffraction_angles = np.nan_to_num(Diffraction_angles)
        Atomic_scattering_factors = get_atomic_scattering_factor(Diffraction_angles, Lambda, Atomic_scattering_factor_parameter_a, Atomic_scattering_factor_parameter_b, Atomic_scattering_factor_parameter_c, Source_beam)
        
        Structure_factor, Intensities = get_structure_factor_intensities(Reciprocal_atom_coordinates, Real_atom_coordinates, Atomic_scattering_factors)
        
        ##### ONLY FOR TESTING #####
        Miller_indices_sum = (np.sum(Reciprocal_atom_coordinates,axis = 1) % 2)
        
        ### Checks whether only the even indices are diffracted planes ###
        assert (Miller_indices_sum == Miller_indices_sum[0]).all()
        
        ### Checks whether Intensities = 4*Atomic scattering factor^2 ###
        assert np.allclose(Intensities,4*Atomic_scattering_factors**2)
        
    print("\n\n######################## BCC - TEST PASSED ########################\nOnly even (hkl) planes are diffracted and Intensties = 4*(Atomic scattering factor)^2 !")
        
        
        
    #     if Source_beam =="electron":
    #         Intensities = Intensities / No_of_atoms # For electron diffraction, intensities are normalized by the no of atoms in the simulation cell
            
        
    #     ##################################### Diffraction coordinates on the diffraction screen #####################################

    #     Reciprocal_atom_coordinates_extrapolated = (Diffracted_wave_vector + ((Diffracted_wave_vector / (np.repeat(Diffracted_wave_vector[:,0], 3).reshape(-1,3))) * Ewald_sphere_radius)) + Reciprocal_atom_coordinates # Extrapolating the diffracted wave vector to a distance of Ewald_sphere_radius where the diffraction screen is virtually placed 

    #     """
    #     =============================================================================================================================================================================================================
    #                                                                             Diffraction Pattern Plot
    #     =============================================================================================================================================================================================================
    #     """

    #     Coordinates_in_indices = np.around((Reciprocal_atom_coordinates / Unit_value_reciprocal_space)) # Finds the miller indices in reciprocal space
    #     Diffracting_coordinates_miller_indices = np.append(Diffracting_coordinates_miller_indices,(Reciprocal_atom_coordinates_all[Intersecting_indices] / Unit_value_reciprocal_space))
    #     Diffraction_angles_of_diffracting_coordinates = np.append(Diffraction_angles_of_diffracting_coordinates, Diffraction_angles)
    #     Intensities_of_diffracting_coordinates = np.append(Intensities_of_diffracting_coordinates, Intensities)
    #     Non_zero_intensities = Intensities[Intensities > 1]

    #     Miller_index = ((Reciprocal_atom_coordinates_all[Intersecting_indices] / Unit_value_reciprocal_space)).astype(int)
    #     # Miller_index = Miller_index / np.amax(Miller_index)
    #     # Miller_index = np.nan_to_num(Miller_index).reshape(-1,3)
    #     Reciprocal_intersecting = Reciprocal_atom_coordinates_all[Intersecting_indices]
        
    
    #     p1 = ax.scatter(Reciprocal_atom_coordinates_extrapolated[:,1],Reciprocal_atom_coordinates_extrapolated[:,2], c = Intensities, cmap = "rainbow", vmin = 0, vmax = 100, s = Intensities*2)

    # Ewald_circle = plt.Circle((0,0), Ewald_sphere_radius, label = "Ewald circle", fill = False)
    # ax.add_artist(Ewald_circle)
    # plt.colorbar(p1, label = "Intensities (arbitrary units)")
    # # ax.set(xlabel = "y coordinate " + r"($\frac{1}{\AA}$)", ylabel = "z coordinate " + r"($\frac{1}{\AA}$)", xlim = [-4,4], ylim = [-4,4])
    # ax.set( xlim = [-4,4], ylim = [-4,4])
    # ax.set_title("Virtual {} diffraction pattern Al {} Lambda {} Zone_axis = [100]".format(Source_beam,Lattice_constant,Lambda), weight = "bold")
    # ax.grid()
    # # ax.legend()
    # Diff_pattern.savefig("Virtual_diffraction_pattern_Al_{}_{}_Lambda_{}.png".format(Lattice_constant,Source_beam,Lambda))
